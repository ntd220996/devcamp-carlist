import { gObjectCars } from './info.js'



function App() {
  return (
    <div>
      {/* Thêm tên và biển số */}
      <ul>
        {gObjectCars.map(function( element, index ) {
          return <li key={index}> { 'Xe: ' + element.make + ' - ' + ' Biển số: ' + element.vID + ' - ' + ' Trạng thái: ' +
           (element.year >= 2018 ? 'Ô tô mới' : 'Ô tô cũ')  } </li>
        }) }
      </ul>

    </div>
  );
}

export default App;
